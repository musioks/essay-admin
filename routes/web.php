<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@login')->name('login');
Route::post('/login', 'IndexController@signin');
Route::get('/join', 'RegisterController@index')->name('register');
Route::post('/join', 'RegisterController@store');
Route::get('/activate-email/{user}', 'RegisterController@verify_email')->name('activate-email');
Route::get('email/verify', 'RegisterController@resend');
Route::get('email/resend', 'RegisterController@resend_verification')->name('verification.resend');
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'Auth\RegisterController@register');
Route::post('/signout', 'IndexController@getLogout')->name('signout');

/** ===========================Password Reset Routes=============================*/
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
//Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
//Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
/** ===========================End Password Reset Routes=============================*/

/** =========================== Member Routes=============================*/
Route::prefix('dashboard')->middleware('auth', 'active')->group(function () {
    Route::get('/', 'Admin\IndexController@index')->middleware('admin');
    Route::get('/orders', 'Admin\OrderController@index')->middleware('admin');
    Route::get('/active-orders', 'Admin\OrderController@active_orders')->middleware('admin');
    Route::get('/completed-orders', 'Admin\OrderController@complete_orders')->middleware('admin');
    Route::get('/order-details/{id}', 'Admin\OrderController@order')->middleware('admin');
    Route::get('/payments', 'Admin\OrderController@payments')->middleware('admin');
    Route::post('/upload-complete-order', 'Admin\OrderController@upload_order')->middleware('admin');
    Route::get('/complete-order/{id}', 'Admin\OrderController@order_complete')->middleware('admin');

});


/** ===========================Dashboard Routes=============================*/
Route::prefix('admin')->middleware('auth', 'active')->group(function () {
    Route::get('/profile', 'Admin\IndexController@profile')->middleware('admin');
    Route::post('/profile', 'Admin\IndexController@updateProfile')->middleware('admin');
    Route::get('/password', 'Admin\IndexController@getPassword')->middleware('admin');
    Route::post('/password', 'Admin\IndexController@changePassword')->middleware('admin');
    Route::get('/', 'Admin\IndexController@index')->middleware('admin');
    // ----------------Extras ---


});
/**========================= end Dashboard routes=================================*/

