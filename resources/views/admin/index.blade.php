@extends('layouts.master')
@section('crumbs')
    <li class="breadcrumb-item active">Dashboard</li>
@endsection
@section('title')
    Dashboard
@endsection
@section('content')

    <div class="row">
        <div class="col-xl-3">
            <a href="{{url('/dashboard/orders')}}">
                <div class="card-box widget-chart-one gradient-danger bx-shadow-lg">
                    <div class="float-left"><i class="fa fa-cart-plus fa-5x text-white"></i></div>
                    <div class=" text-center"><p class="text-white mb-0 mt-2">
                            All Orders</p>
                        <h3 class="text-white">{{$orders->count() ?? ''}}</h3></div>
                </div>
            </a>
        </div><!-- end col -->
        <div class="col-xl-3">
            <a href="{{url('/dashboard')}}">
                <div class="card-box widget-chart-one gradient-info bx-shadow-lg">
                    <div class="float-left"><i class="fa fa-users fa-5x text-white"></i></div>
                    <div class="text-center"><p class="text-white mb-0 mt-2">
                            Customers</p>
                        <h3 class="text-white">{{$customers->count() ?? ''}}</h3></div>
                </div>
            </a>
        </div><!-- end col -->
        <div class="col-xl-3">
            <a href="{{url('/dashboard/active-orders')}}">
                <div class="card-box widget-chart-one gradient-success bx-shadow-lg">
                    <div class="float-left"><i class="fa fa-table fa-5x text-white"></i></div>
                    <div class="text-center"><p class="text-white mb-0 mt-2">
                            Active Orders</p>
                        <h3 class="text-white">{{$orders->where('status','Paid')->where('completed',0)->count() ?? ''}}</h3></div>
                </div>
            </a>
        </div><!-- end col -->
        <div class="col-xl-3">
            <a href="{{url('/dashboard/completed-orders')}}">
            <div class="card-box widget-chart-one gradient-info bx-shadow-lg">
                <div class="float-left clearfix"><i class="fa fa-check-square-o fa-5x text-white"></i></div>
                <div class=" text-center"><p class="text-white mb-0 mt-2">
                        Complete Orders</p>
                    <h3 class="text-white">{{$orders->where('status','Paid')->where('completed',1)->count() ?? ''}}</h3></div>
            </div>
            </a>
        </div><!-- end col -->
    </div><!-- end row -->
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="clearfix"></div>
                <!-- fetch courses -->
                <div class="table-responsive">
                    <table class="table table-bordered table-sm" id="datatable">
                        <thead class="bg-info text-center text-white">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Customer Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Country</th>
                            <th scope="col">Date Joined</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($customers as $i=>$customer)
                            <tr>
                                <td>{{$i+1}}</td>
                                <td>{{$customer->full_name ?? ''}}</td>
                                <td>{{$customer->email ?? ''}}</td>
                                <td>{{$customer->phone ?? '-'}}</td>
                                <td>{{$customer->country ?? '-'}}</td>
                                <td>{{date('d M-Y',strtotime($customer->created_at)) ?? ''}}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div><!--end responsive table-->

            </div>
        </div>
    </div>

@endsection
