@extends('layouts.master')
@section('crumbs')
    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active">All Payments</li>
@endsection

@section('title') All Order payments .
@endsection

@section('content')
    <div class="row">
        <div class="col-xl-6">
            <div class="card-box widget-chart-one gradient-success bx-shadow-lg">
                <div class="float-left"><i class="mdi mdi-cash-usd fa-5x text-white"></i></div>
                <div class=" text-center"><h4 class="text-white mb-0 mt-2">
                        Total Amount paid</h4>
                    <h3 class="text-white">@php $paid=$payments->where('paid',1)->pluck('order_total')->sum() @endphp
                   $@convert($paid,2)
                    </h3></div>
            </div>
        </div><!-- end col -->
        <div class="col-xl-6">
            <div class="card-box widget-chart-one gradient-danger bx-shadow-lg">
                <div class="float-left"><i class="mdi mdi-wallet fa-5x text-white"></i></div>
                <div class="text-center"><h4 class="text-white mb-0 mt-2">
                        Pending Payments' total </h4>
                    <h3 class="text-white">@php $unpaid=$payments->where('paid',0)->pluck('order_total')->sum(); @endphp
                        $@convert($unpaid,2)
                    </h3></div>
            </div>
        </div><!-- end col -->


    </div><!-- end row -->
    <!--  ================= Start on displaying payments on table =========== -->
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="clearfix"></div>
                <!-- fetch courses -->
                <div class="table-responsive">
                    <table class="table table-bordered table-sm" id="datatable">
                        <thead class="bg-info text-center text-white">
                        <tr>
                            <th>#</th>
                            <th>Order No.</th>
                            <th>Customer</th>
                            <th>Order Date</th>
                            <th>Amount in $</th>
                            <th>Payment Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($payments as $i=> $payment)
                            <tr>
                                <td>{{$i+1}}</td>
                                <td>{{$payment->order_id ?? ''}}</td>
                                <td>{{$payment->order->customer->full_name ?? ''}}</td>
                                <td>{{$payment->order->created_at ?? ''}}</td>
                                <td>$@convert($payment->order_total,2)</td>
                                <td>
                                    @if($payment->paid==false)
                                        <span class="badge badge-danger p-2"><i class="fa fa-times"></i> Unpaid</span>
                                    @else
                                        <span class="badge badge-success p-2"><i class="fa fa-check"></i> Paid</span>
                                    @endif
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end responsive table-->

            </div>
        </div>
    </div>
@endsection
