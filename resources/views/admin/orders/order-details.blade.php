@extends('layouts.master')
@section('styles')
    <style>
        .switch {
            font-size: 1rem;
            position: relative;
        }

        .switch input {
            position: absolute;
            height: 1px;
            width: 1px;
            background: none;
            border: 0;
            clip: rect(0 0 0 0);
            clip-path: inset(50%);
            overflow: hidden;
            padding: 0;
        }

        .switch input + label {
            position: relative;
            min-width: calc(calc(2.375rem * .8) * 2);
            border-radius: calc(2.375rem * .8);
            height: calc(2.375rem * .8);
            line-height: calc(2.375rem * .8);
            display: inline-block;
            cursor: pointer;
            outline: none;
            user-select: none;
            vertical-align: middle;
            text-indent: calc(calc(calc(2.375rem * .8) * 2) + .5rem);
        }

        .switch input + label::before,
        .switch input + label::after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: calc(calc(2.375rem * .8) * 2);
            bottom: 0;
            display: block;
        }

        .switch input + label::before {
            right: 0;
            background-color: #dee2e6;
            border-radius: calc(2.375rem * .8);
            transition: 0.2s all;
        }

        .switch input + label::after {
            top: 2px;
            left: 2px;
            width: calc(calc(2.375rem * .8) - calc(2px * 2));
            height: calc(calc(2.375rem * .8) - calc(2px * 2));
            border-radius: 50%;
            background-color: white;
            transition: 0.2s all;
        }

        .switch input:checked + label::before {
            background-color: #08d;
        }

        .switch input:checked + label::after {
            margin-left: calc(2.375rem * .8);
        }

        .switch input:focus + label::before {
            outline: none;
            box-shadow: 0 0 0 0.2rem rgba(0, 136, 221, 0.25);
        }

        .switch input:disabled + label {
            color: #868e96;
            cursor: not-allowed;
        }

        .switch input:disabled + label::before {
            background-color: #e9ecef;
        }

        .switch.switch-sm {
            font-size: 0.875rem;
        }

        .switch.switch-sm input + label {
            min-width: calc(calc(1.9375rem * .8) * 2);
            height: calc(1.9375rem * .8);
            line-height: calc(1.9375rem * .8);
            text-indent: calc(calc(calc(1.9375rem * .8) * 2) + .5rem);
        }

        .switch.switch-sm input + label::before {
            width: calc(calc(1.9375rem * .8) * 2);
        }

        .switch.switch-sm input + label::after {
            width: calc(calc(1.9375rem * .8) - calc(2px * 2));
            height: calc(calc(1.9375rem * .8) - calc(2px * 2));
        }

        .switch.switch-sm input:checked + label::after {
            margin-left: calc(1.9375rem * .8);
        }

        .switch.switch-lg {
            font-size: 1.25rem;
        }

        .switch.switch-lg input + label {
            min-width: calc(calc(3rem * .8) * 2);
            height: calc(3rem * .8);
            line-height: calc(3rem * .8);
            text-indent: calc(calc(calc(3rem * .8) * 2) + .5rem);
        }

        .switch.switch-lg input + label::before {
            width: calc(calc(3rem * .8) * 2);
        }

        .switch.switch-lg input + label::after {
            width: calc(calc(3rem * .8) - calc(2px * 2));
            height: calc(calc(3rem * .8) - calc(2px * 2));
        }

        .switch.switch-lg input:checked + label::after {
            margin-left: calc(3rem * .8);
        }

        .switch + .switch {
            margin-left: 1rem;
        }

        .dropdown-menu {
            margin-top: .75rem;
        }
    </style>
@endsection
@section('crumbs')
    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Order details</li>
@endsection
@section('title')
    Order details
@endsection
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="clearfix"></div>
                <ul class="nav nav-pills mb-3 border-bottom pb-1" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
                           aria-controls="pills-home" aria-selected="true">Order Details</a>
                    </li>
                    @if($order->completed==0 && $order->payment->paid==1)
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
                               role="tab" aria-controls="pills-profile" aria-selected="false">Upload Complete Order</a>
                        </li>
                    @else
                        {{""}}
                    @endif
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                         aria-labelledby="pills-home-tab">
                        <div class="card-deck">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Order Title</h5>
                                    <p class="card-text border-bottom indigo">{{$order->topic ?? ''}}</p>
                                    <h5 class="card-title">Service Type</h5>
                                    <p class="card-text border-bottom indigo">{{$order->service_type->name ?? ''}}</p>
                                    <h5 class="card-title">Paper Type</h5>
                                    <p class="card-text border-bottom indigo">{{$order->paper_type->name ?? ''}}</p>
                                    <h5 class="card-title">Academic Level</h5>
                                    <p class="card-text border-bottom indigo">{{$order->academic_level->name ?? ''}}</p>
                                    <h5 class="card-title">Subject</h5>
                                    <p class="card-text border-bottom indigo">{{$order->subject->name ?? ''}}</p>
                                    <h5 class="card-title">Citation Style</h5>
                                    <p class="card-text  indigo">{{$order->citation->name ?? ''}}</p>
                                </div>
                            </div>
                            <div class="card bg-light">
                                <div class="card-body">
                                    <h5 class="card-title">Deadline</h5>
                                    <p class="card-text border-bottom indigo">{{$order->deadline->name ?? ''}}</p>
                                    <h5 class="card-title">No of Pages</h5>
                                    <p class="card-text border-bottom indigo">{{$order->no_of_pages ?? ''}}</p>
                                    <h5 class="card-title">Order Total</h5>
                                    <p class="card-text"><span
                                            class="badge badge-pill badge-primary p-2">$@convert($order->order_total,2)</span>
                                    </p>
                                    <h5 class="card-title">Order Status</h5>
                                    <p class="card-text border-bottom orange">{{$order->order_status->name ?? ''}}</p>
                                    <h5 class="card-title">Payment Status</h5>
                                    <p class="card-text border-bottom purple">{{($order->payment->paid==1) ? 'Paid' : 'Not Paid'}}</p>
                                    <h5 class="card-title">Order Instructions</h5>
                                    <p class="card-text indigo">{{$order->paper_details ?? ''}}</p>
                                </div>
                            </div>

                        </div>
                        <div class="card mt-4" style="width: 35rem;">
                            <div class="card-header bg-success text-white">
                                <h2>Order Attachments</h2>
                            </div>
                            <ul class="list-group list-group-flush">
                                @foreach($order->attachments as $upload)
                                    <li class="list-group-item">
                                        <strong class="orange">{{$upload->attachment_type->name}}</strong> :
                                        <a href="http://essay.io/orders/attachments/{{$upload->document}}"> <i
                                                class="ti-files"></i> {{$upload->document ?? ''}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <form action="{{url('/dashboard/upload-complete-order')}}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <h3 class=" text-success border-bottom">Upload details of the complete order below</h3>
                            <input type="hidden" name="order_id" value="{{$order->id}}">
                            <div class="form-group">
                                <label for="topic1">&nbsp;</label>
                                <div id="dynamic">
                                    <div class="row mb-1">
                                        <div class="col-sm-10">
                                            <input type="file" name="document[]" class="form-control" required>
                                        </div>
                                        <div class="col-sm-2">
                                            <button type="button" name="add" class="btn btn-success" id="add_input">
                                                <i
                                                    class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Large switch -->
                            <div class="form-group">
                                <h5>Is this Order Complete?</h5>
                                <span class="switch switch-lg">
                            <label for="switch-lg" class="text-danger">OFF</label>
                            <input type="checkbox" class="switch" name="switch" id="switch-lg" checked>
                            <label for="switch-lg" class="text-success">ON</label>
                            </span>
                            </div>
                            <div class="form-group text-center border-top pt-1">
                                <button type="submit" class="btn btn-success btn-lg">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- show original order details -->

                <!-- show complete order details -->


            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            let i = 1;
            $('#add_input').click(function () {
                i++;
                $('#dynamic').append('<div class="row mb-1" id="row' + i + '"> <td style="display:none;">' +
                    '<div class="col-sm-10">\n' +
                    '                                            <input type="file" class="form-control"\n' +
                    '                                                   name="document[]" required>\n' +
                    '                                    </div>\n' +
                    '<div class="col-sm-2"><button type="button" name="remove" class="btn btn-danger btn_remove" id="' +
                    i + '"><i class="fa fa-minus"></i></button></div></div>');
            });
            $(document).on('click', '.btn_remove', function () {
                let button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });
        });
    </script>
@endsection
