@extends('layouts.master')
@section('crumbs')
    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active"> Complete Order details</li>
@endsection
@section('title')
    <span class="text-success">Complete Order details</span>
@endsection
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="alert alert-success" role="alert">
                This order no.  <a href="#" class="alert-link">#ES1/{{date('Y')}}/{{$order->id}}</a> is complete.
            </div>
            <div class="card-box">
                <div class="clearfix"></div>
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Order Title</h5>
                            <p class="card-text border-bottom indigo">{{$order->topic ?? ''}}</p>
                            <h5 class="card-title">Service Type</h5>
                            <p class="card-text border-bottom indigo">{{$order->service_type->name ?? ''}}</p>
                            <h5 class="card-title">Paper Type</h5>
                            <p class="card-text border-bottom indigo">{{$order->paper_type->name ?? ''}}</p>
                            <h5 class="card-title">Academic Level</h5>
                            <p class="card-text border-bottom indigo">{{$order->academic_level->name ?? ''}}</p>
                            <h5 class="card-title">Subject</h5>
                            <p class="card-text border-bottom indigo">{{$order->subject->name ?? ''}}</p>
                            <h5 class="card-title">Citation Style</h5>
                            <p class="card-text  indigo">{{$order->citation->name ?? ''}}</p>
                        </div>
                    </div>
                    <div class="card bg-light">
                        <div class="card-body">
                            <h5 class="card-title">Deadline</h5>
                            <p class="card-text border-bottom indigo">{{$order->deadline->name ?? ''}}</p>
                            <h5 class="card-title">No of Pages</h5>
                            <p class="card-text border-bottom indigo">{{$order->no_of_pages ?? ''}}</p>
                            <h5 class="card-title">Order Total</h5>
                            <p class="card-text"><span
                                    class="badge badge-pill badge-primary p-2">$@convert($order->order_total,2)</span>
                            </p>
                            <h5 class="card-title">Order Status</h5>
                            <p class="card-text border-bottom orange">{{$order->order_status->name ?? ''}}</p>
                            <h5 class="card-title">Payment Status</h5>
                            <p class="card-text border-bottom purple">{{($order->payment->paid==1) ? 'Paid' : 'Not Paid'}}</p>
                            <h5 class="card-title">Order Instructions</h5>
                            <p class="card-text indigo">{{$order->paper_details ?? ''}}</p>
                        </div>
                    </div>

                </div>
                <div class="card mt-4 text-center">
                    <div class="card-header bg-dark text-white">
                        <h4>View Uploaded Documents</h4>
                    </div>
                    <ul class="list-group list-group-flush">
                        @foreach($order->completed_orders as $upload)
                            <li class="list-group-item">
                                <i class="fa fa-file-o fa-2x fa-fw"></i> {{$upload->document}}   <a
                                    href="{{asset('orders/'.$upload->document)}}">  <i class="fa fa-download"></i>
                                    Download</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

        @endsection
        @section('scripts')
            <script>
                $(document).ready(function () {
                    let i = 1;
                    $('#add_input').click(function () {
                        i++;
                        $('#dynamic').append('<div class="row mb-1" id="row' + i + '"> <td style="display:none;">' +
                            '<div class="col-sm-10">\n' +
                            '                                            <input type="file" class="form-control"\n' +
                            '                                                   name="document[]" required>\n' +
                            '                                    </div>\n' +
                            '<div class="col-sm-2"><button type="button" name="remove" class="btn btn-danger btn_remove" id="' +
                            i + '"><i class="fa fa-minus"></i></button></div></div>');
                    });
                    $(document).on('click', '.btn_remove', function () {
                        let button_id = $(this).attr("id");
                        $('#row' + button_id + '').remove();
                    });
                });
            </script>
@endsection
