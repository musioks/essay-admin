@extends('layouts.master')
@section('crumbs')
    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Active Orders</li>
@endsection
@section('title')
    Orders in Progress
@endsection
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="clearfix"></div>
                <!-- fetch courses -->
                <div class="table-responsive">
                    <table class="table table-bordered table-sm" id="datatable">
                        <thead class="bg-info text-center text-white">
                        <tr>
                            <th>#</th>
                            <th>Service</th>
                            <th>Topic</th>
                            <th>Deadline</th>
                            <th>No. of Pages</th>
                            <th>Total</th>
                            <th>Order Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $i=> $order)
                            <tr>
                                <td>{{$i+1}}</td>
                                <td>{{$order->service_type ?? ''}}</td>
                                <td>{{$order->topic ?? ''}}</td>
                                <td>{{$order->deadline ?? ''}}</td>
                                <td>{{$order->pages ?? ''}}</td>
                                <td>$@convert($order->total,2)</td>
                                <td>{{$order->status ?? ''}}</td>
                                <td>
                                    <a href="{{url('/dashboard/order-details/'.$order->id)}}" class="btn btn-info">More details</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!--end responsive table-->

            </div>
        </div>
    </div>

@endsection
