<div class="left-side-menu left-side-menu-dark">
    <div class="slimscroll-menu"><!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <li class="menu-title">Navigation</li>
                <li><a href="{{url('/dashboard')}}" class="{{Request::is('dashboard') ? 'active' :''}}"><i class="mdi mdi-view-dashboard"></i> <span>Dashboard</span></a></li>
                <li><a href="{{url('/dashboard/active-orders')}}" class="{{Request::is('dashboard/active-orders') ? 'active' :''}}"><i class="mdi mdi-atom"></i> <span> Active Orders</span></a></li>
                <li><a href="{{url('/dashboard/completed-orders')}}" class="{{Request::is('dashboard/completed-orders') ? 'active' :''}}"><i class="mdi mdi-file-document-box"></i> <span>Completed Orders</span></a></li>
                <li><a href="{{url('/dashboard/orders')}}" class="{{Request::is('dashboard/orders') ? 'active' :''}}"><i class="mdi mdi-approval"></i> <span>All Orders</span></a></li>
                <li><a href="{{url('/dashboard/payments')}}" class="{{Request::is('dashboard/payments') ? 'active' :''}}"><i class="mdi mdi-cash-usd"></i> <span>Payments</span></a></li>
            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>
