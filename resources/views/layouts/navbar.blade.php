<header id="topnav" class="topbar-light">
    <nav class="navbar-custom bg-light border-bottom border-light">
        <ul class="list-unstyled topbar-right-menu float-right mb-0">

            <li class="dropdown notification-list"><a class="nav-link dropdown-toggle nav-user"
                                                      data-toggle="dropdown" href="#" role="button"
                                                      aria-haspopup="false" aria-expanded="false">
                    @if(auth()->user()->avatar=='')
                        {{--<img src="{{ \Avatar::create(auth()->user()->member->fname)->toBase64() }}" alt="user" class="rounded-circle" />--}}
                        <img src="{{asset('/assets/images/users/user.png')}}" alt="user" class="rounded-circle">
                    @else
                        <img src="{{asset('/assets/images/users/'.auth()->user()->avatar)}}" alt="user"
                             class="rounded-circle">
                    @endif
                    <span class="ml-1 text-white"> {{auth()->user()->name}} <i
                            class="mdi mdi-chevron-down"></i></span></a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown"><!-- item-->
                    <!-- item--> <a href="{{url('/admin/profile')}}" class="dropdown-item notify-item"><i
                            class="fa fa-fw fa-user"></i> <span>My Profile</span> </a><!-- item--> <a
                        href="{{url('/admin/password')}}" class="dropdown-item notify-item"><i
                            class="fa fa-fw fa-gear"></i> <span>Change Password</span> </a><!-- item-->

                    <a href="{{ route('signout') }}" class="dropdown-item notify-item"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fa fa-fw fa-power-off"></i> <span>Logout</span>
                    </a>

                    <form id="logout-form" action="{{ route('signout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
        <ul class="list-unstyled menu-left mb-0">
            <li class="float-left border-bottom "><a href="{{url('/dashboard')}}" class="logo ">
              <span class="logo-lg">
<img src="{{asset('logo.png')}}" alt="">
              </span>
                    <img class="logo-sm" style="width:68px;height:48px;" src="{{asset('logo.png')}}"
                         alt=""></a></li>
            <li class="float-left"><a class="button-menu-mobile open-left navbar-toggle">
                    <div class="lines text-white"><span></span> <span></span> <span></span></div>
                </a></li>

        </ul>
    </nav>
    <!-- end navbar-custom -->
</header>
