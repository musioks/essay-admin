@include('layouts.header')
{{--<body class="bg-account-pages">--}}
<body style="background: #2C7C64;">
<!-- Login -->
<section>
    <div class="container">
        <div class="row">
            <div class="wrapper-page mt-5">
                <div class="col-md-6 offset-md-3 bg-white rounded-0"><!-- Logo box-->
                    <div class="account-logo-box border-bottom mt-0">
                        <h2 class="text-uppercase text-center mt-0">
                            <a href="{{ route('register') }}" class="text-success"><span><img
                                        src="{{asset('assets/images/axil.png')}}" alt=""
                                        style="width:130px;"></span></a>
                        </h2>
                    </div>
                    <div class="account-content mt-0 mb-5 pb-5">
                        <form role="form" action="{{ route('register') }}" method="post">
                            @csrf
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="form-username">Full Name <span class="text-danger">*</span></label>
                                        <input type="text" name="name" placeholder="Full name"
                                               class=" form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                               value="{{ old('name') }}" required>
                                    </div>
                                </div><!--end col-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="form-username">Email Address <span
                                                class="text-danger">*</span></label>
                                        <input type="email" name="email" placeholder="Enter Email"
                                               class="form-control" value="{{ old('email') }}" required>
                                    </div>
                                </div><!--end col-->
                            </div><!-- end row-->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="form-username">Password<span
                                                class="text-danger">*</span></label>
                                        <input type="password" name="password" placeholder="Enter Password"
                                               class="form-control" required>
                                    </div>
                                </div><!--end col-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="form-username">Confirm Password</label>
                                        <input type="password" name="password_confirmation"
                                               placeholder="Confirm Password"
                                               class="form-control">
                                    </div>
                                </div><!--end col-->
                            </div><!-- end row-->
                            <div class="text-center">
                                <button type="submit" class="btn btn-info btn-block btn-lg">JOIN NOW
                                </button>
                            </div>
                            <a href="{{ url('/')}}"
                               class="text-muted float-right mt-2">
                                <p>Have an account? <em class="flip" style="color: #0D13F5;">Click here to Login</em>
                                </p>
                            </a>
                        </form>

                        <!-- end form -->

                    </div><!-- end account-content -->

                </div><!-- end wrapper-page -->
            </div>
        </div>
        <!-- end row -->
    </div><!-- end container -->
</section><!-- END HOME -->
<!-- jQuery  -->
@include('layouts.scripts')
</body>
</html>
