<?php
/**
 * Created by PhpStorm.
 * User: Silicon
 * Date: 1/14/2019
 * Time: 10:31 PM
 */

namespace App\Http\Controllers\Helpers;


use App\Orders\Order;

class DisplayOrder
{
    public static function order()
    {
        $gross_orders = Order::latest()->get();
        //dd($gross_orders);
        $gross_orders->transform(function ($item) {
            return [
                'id' => $item->id,
                'customer' => $item->customer->full_name,
                'service_type' => $item->service_type->name,
                'paper_type' => $item->paper_type->name,
                'citation' => $item->citation->name,
                'academic_level' => $item->academic_level->name,
                'subject' => $item->subject->name,
                'deadline' => $item->deadline->name,
                'total' => $item->order_total,
                'pages' => $item->no_of_pages,
                'status' => $item->order_status->name,
                'topic' => $item->topic,
                'details' => $item->paper_details,
                'completed' => $item->completed,
                'order_date' => $item->created_at,
            ];
        });
        //  dd($gross_orders);
        $orders = collect(json_decode(json_encode($gross_orders, FALSE)));
        return $orders;
    }
}
