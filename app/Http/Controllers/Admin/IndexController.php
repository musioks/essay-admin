<?php

namespace App\Http\Controllers\Admin;


use App\Customer;
use App\Orders\Order;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class IndexController extends Controller
{
    public function index()
    {
        $gross_orders = Order::latest()->get();
        //dd($gross_orders);
        $gross_orders->transform(function ($item) {
            return [
                'id' => $item->id,
                'customer' => $item->customer->full_name,
                'service_type' => $item->service_type->name,
                'paper_type' => $item->paper_type->name,
                'citation' => $item->citation->name,
                'academic_level' => $item->academic_level->name,
                'subject' => $item->subject->name,
                'deadline' => $item->deadline->name,
                'total' => $item->order_total,
                'pages' => $item->no_of_pages,
                'status' => $item->order_status->name,
                'topic' => $item->topic,
                'details' => $item->paper_details,
                'completed' => $item->completed,
                'order_date' => $item->created_at,
            ];
        });
        //  dd($gross_orders);
        $orders = collect(json_decode(json_encode($gross_orders, FALSE)));
      // dd($orders);
        $customers = Customer::latest()->get();
        return view('admin.index')->with(compact('orders', 'customers'));
    }

    public function profile()
    {
        $user = Auth::user();
        return view('admin.profile', ['user' => $user]);
    }

    public function getPassword()
    {
        $user = Auth::user();
        return view('admin.password', ['user' => $user]);
    }

    public function updateProfile(Request $request)
    {
        $id = Auth::user()->id;
        $user = User::find($id);
        // dd($request->all());
        $user->update(array_merge($request->all()));
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $fileName = time() . '.' . $file->getClientOriginalExtension();
            $request->avatar->move('assets/images/users/', $fileName);
            $user->update(['avatar' => $fileName]);
        }
        return redirect()->back()->with('success', 'Profile has been updated!');

    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'cpassword' => 'required',
            'password' => 'required|confirmed',
        ]);
        if (!(Hash::check($request->get('cpassword'), Auth::user()->password))) {
// The passwords matches
            return redirect()->back()->with('error', 'Your current password does not match with the password you provided. Please try again.!');
        }
        if (strcmp($request->get('cpassword'), $request->get('password')) == 0) {
//Current password and new password are same
            return redirect()->back()->with('error', 'New Password cannot be same as your current password. Please choose a different password!');
        }

//Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('password'));
        $user->save();
        return redirect()->back()->with('success', 'Password changed successfully!');
    }
}
