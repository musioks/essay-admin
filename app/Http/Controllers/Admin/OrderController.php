<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Helpers\DisplayOrder;
use App\Orders\Completed_order;
use App\Orders\Order;
use App\Orders\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;

class OrderController extends Controller
{
    public function index()
    {
        /**
         *display all orders
         */
        $orders = DisplayOrder::order();
        //dd($orders);
        return view('admin.orders.index')->with(compact('orders'));

    }

    public function active_orders()
    {
        /**
         *display all active orders
         */
        $orders = DisplayOrder::order()->where('status', 'Paid')->where('completed', 0);
        // dd($orders);
        return view('admin.orders.active')->with(compact('orders'));

    }

    public function complete_orders()
    {
        /**
         *display all  complete orders
         */
        $orders = DisplayOrder::order()->where('status', 'Paid')->where('completed', 1);
        //dd($orders);
        return view('admin.orders.completed')->with(compact('orders'));
    }

    public function order($id)
    {
        /**
         *display individual  order details
         */
        $order = Order::find($id);
        //dd($order);
        return view('admin.orders.order-details')->with(compact('order'));
    }

    public function payments()
    {
        /**
         *display all  order payments
         */
        $payments = Payment::latest()->get();
        //dd($payments);
        return view('admin.payments')->with(compact('payments'));
    }

    public function upload_order(Request $request)
    {
        // dd($request->all());
        if ($request->switch == "on") {
            $data = [];
            if ($request->hasfile('document')) {

                foreach ($request->file('document') as $file) {
                    $name = time() . '.' . $file->getClientOriginalName();
                    $file->move('orders/', $name);
                    $data[] = $name;
                }
            }
            $rows = (int)collect($data)->count();
            // dd($rows);
            for ($j = 0; $j < $rows; $j++) {
                Completed_order::insert([
                    'order_id' => $request->order_id,
                    'document' => $data[$j],
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
            }
            Order::where('id', $request->order_id)->update(['completed' => 1]);
            return redirect()->back()->with('success', 'Order has been completed successfully!');
        } else {
            return redirect()->back()->with('warning', "You must switch the complete button to `ON` to complete order!, please try again!");
        }
    }

    public function order_complete($id)
    {
        /**
         *display individual  complete order details
         */
        $order = Order::find($id);
        //dd($order);
        return view('admin.orders.complete_details')->with(compact('order'));
    }
}
