<?php

namespace App;

use App\Orders\Order;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    protected $fillable =
        [
            'user_id',
            'full_name',
            'phone',
            'country',
            'email',
        ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'customer_id');
    }
}
