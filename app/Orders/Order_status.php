<?php

namespace App\Orders;

use Illuminate\Database\Eloquent\Model;

class Order_status extends Model
{
    protected $table = 'order_statuses';

    protected $fillable = ['name'];

    public function orders()
    {
        return $this->hasMany(Order::class, 'status_id');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, 'status_id');
    }
}
