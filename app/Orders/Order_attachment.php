<?php

namespace App\Orders;

use App\Settings\Attachment_type;
use Illuminate\Database\Eloquent\Model;

class Order_attachment extends Model
{
    protected $table = 'order_attachments';

    protected $fillable = [
        'order_id',
        'attachment_type_id',
        'document',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function attachment_type()
    {
        return $this->belongsTo(Attachment_type::class, 'attachment_type_id');
    }
}
