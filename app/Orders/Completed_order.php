<?php

namespace App\Orders;

use Illuminate\Database\Eloquent\Model;

class Completed_order extends Model
{
    protected $table = 'completed_orders';

    protected $fillable = [
        'order_id',
        'document',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
}
