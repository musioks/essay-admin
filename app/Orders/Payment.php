<?php

namespace App\Orders;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    protected $fillable = ['order_id', 'customer_id', 'order_total', 'status_id'];


    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function status()
    {
        return $this->belongsTo(Order_status::class, 'status_id');
    }
}
