<?php

namespace App\Orders;

use App\Customer;
use App\Settings\Academic_level;
use App\Settings\Citation;
use App\Settings\Paper_type;
use App\Settings\Service_type;
use App\Settings\Subject;
use App\Settings\Time_matrix;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //order model
    protected $table = 'orders';

    protected $fillable = [
        'customer_id',
        'service_type_id',
        'paper_type_id',
        'citation_id',
        'academic_level_id',
        'deadline_id',
        'subject_id',
        'no_of_pages',
        'topic',
        'paper_details',
        'order_total',
        'status_id',
    ];

    public function completed_orders()
    {
        return $this->hasMany(Completed_order::class, 'order_id');
    }

    public function attachments()
    {
        return $this->hasMany(Order_attachment::class, 'order_id');
    }

    public function payment()
    {
        return $this->hasOne(Payment::class, 'order_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function order_status()
    {
        return $this->belongsTo(Order_status::class, 'status_id');
    }

    public function service_type()
    {
        return $this->belongsTo(Service_type::class, 'service_type_id');
    }

    public function academic_level()
    {
        return $this->belongsTo(Academic_level::class, 'academic_level_id');
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id');
    }

    public function paper_type()
    {
        return $this->belongsTo(Paper_type::class, 'paper_type_id');
    }

    public function citation()
    {
        return $this->belongsTo(Citation::class, 'citation_id');
    }

    public function deadline()
    {
        return $this->belongsTo(Time_matrix::class, 'deadline_id');
    }
}
