<?php

namespace App\Settings;

use App\Orders\Order_attachment;
use Illuminate\Database\Eloquent\Model;

class Attachment_type extends Model
{
    protected $table = 'attachment_types';

    protected $fillable = ['name'];

    public function attachments()
    {
        return $this->hasMany(Order_attachment::class, 'attachment_type_id');
    }
}
