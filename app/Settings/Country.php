<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    public $timestamps = false;
    protected $fillable = ['name', 'calling_code'];
}
