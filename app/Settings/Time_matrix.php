<?php

namespace App\Settings;

use App\Orders\Order;
use Illuminate\Database\Eloquent\Model;

class Time_matrix extends Model
{
    protected $table = 'time_matrices';
    protected $fillable = ['name'];

    public function orders()
    {
        return $this->hasMany(Order::class, 'deadline_id');
    }
}
