<?php

namespace App\Settings;

use App\Orders\Order;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subjects';

    protected $fillable = ['category_id', 'name'];

    public function category()
    {
        return $this->belongsTo(Subject_category::class, 'category_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'subject_id');
    }
}
