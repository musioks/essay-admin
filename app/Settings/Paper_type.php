<?php

namespace App\Settings;

use App\Orders\Order;
use Illuminate\Database\Eloquent\Model;

class Paper_type extends Model
{
    protected $table = 'paper_types';

    protected $fillable = ['name'];

    public function orders()
    {
        return $this->hasMany(Order::class, 'paper_type_id');
    }
}
