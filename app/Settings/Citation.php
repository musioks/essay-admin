<?php

namespace App\Settings;

use App\Orders\Order;
use Illuminate\Database\Eloquent\Model;

class Citation extends Model
{
    protected $table = 'citations';

    protected $fillable = ['name'];

    public function orders()
    {
        return $this->hasMany(Order::class, 'citation_id');
    }
}
