<?php

namespace App\Settings;

use App\Orders\Order;
use Illuminate\Database\Eloquent\Model;

class Service_type extends Model
{
    protected $table = 'service_types';

    protected $fillable = ['name'];

    public function orders()
    {
        return $this->hasMany(Order::class, 'service_type_id');
    }
}
