<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class Subject_category extends Model
{
    protected $table = 'subject_categories';

    protected $fillable = ['name'];

    public function subjects()
    {
        return $this->hasMany(Subject::class, 'category_id');
    }
}
