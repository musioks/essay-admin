<?php

namespace App\Settings;

use App\Orders\Order;
use Illuminate\Database\Eloquent\Model;

class Academic_level extends Model
{
    protected $table = 'academic_levels';
    protected $fillable = ['name'];

    public function orders()
    {
        return $this->hasMany(Order::class, 'academic_level_id');
    }

}
